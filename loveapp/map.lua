--
--  map.lua
--

require 'middleclass'
require 'vector'
require 'colors'
require 'utility'
require 'spatialhash'
require 'serpent'
require 'rectangle'
require 'astar'

local HC = require 'hardoncollider'

Map = class('Map')


function Map:initialize()
  self.spatialhash = SpatialHash(100)

  self.extent = 20 -- How many cells away from the origin the map extends in each direction
  self.cellSize = 50 -- How large each cell is

  Notifier:listenForMessage('world_click', self)

  self.astar = AStar(self)

  self.collider = HC(100, self.on_collision, self.collision_stop)

  self.shapes = {}
end

function Map:receiveMessage(message, data)
  if message == 'world_click' then
    local position = data

  end
end

function Map:update(dt)
end

function Map:draw()
  -- Texture
  local padding = self.cellSize
  local distance = self.extent

  colors.gray:set()
  for x = padding * distance * - 1, padding * distance, padding do
    for y = padding * distance * - 1, padding * distance, padding do
      love.graphics.point(math.floor(x), math.floor(y))
    end
  end

  -- draw walls
  colors.black:set()

  for index, shape in ipairs(self.shapes) do
    shape:draw('fill')
  end
end

function Map:gridCenter(point)
  return vector(math.floor(point.x / self.cellSize) * self.cellSize + self.cellSize / 2,
                math.floor(point.y / self.cellSize) * self.cellSize + self.cellSize / 2)

end

function Map:pointIsWalkable(point)
  for shape in pairs(self.collider:shapesAt(point:unpack())) do
    return false
  end
  return true
end

function Map:pathBetween(start_point, end_point, range)
  -- Can we just walk in a straight line?
  local lookAt = end_point - start_point
  if not self.collider:intersectsRay(start_point.x, start_point.y, lookAt.x, lookAt.y) then
    return {end_point}
  end

  -- get start and end waypoints
  local start_waypoint = self:visibleWaypointInRange(start_point, range, end_point)
  if start_waypoint == nil then
    print('no visible waypoint, getting any nearest')
    start_waypoint = self:anyWaypointInRange(start_point, range, end_point)
  end

  local end_waypoint = self:visibleWaypointInRange(end_point, range)

  if not start_waypoint or not end_waypoint then
    print('neither: '..tostring(start_waypoint) .. ' '..tostring(end_waypoint))
    return nil
  end

  -- get path between waypoints
  local astar_path = self.astar:findPath(start_waypoint, end_waypoint)
  if astar_path == nil then
    print('astar sucked out')
    return nil
  else
    local path = {}
    table.insert(path, start_point)

    for index, node in ipairs(astar_path.nodes) do
      local waypoint = node.location
      table.insert(path, waypoint.circle.position)
    end

    table.insert(path, end_point)

    return path
  end
end

function Map:intersectsRay(origin, delta)
  return self.collider:intersectsRay(origin.x, origin.y, delta.x, delta.y)
end

-- Finds a waypoint near 'point' within 'range', ensures that there is direct line of sight to the waypoint
function Map:visibleWaypointInRange(point, range, target)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local lookAtTarget = nil
  if target ~= nil then
    lookAtTarget = point - target
  end

  local nearest_waypoint = false
  local min_dist = math.huge
  local min_dot = math.huge

  if target ~= nil then
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point
      local dot = lookAt:dot(lookAtTarget)

      if dot < min_dot and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dot = dot
        nearest_waypoint = waypoint
      end
    end
  else
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point

      if dist < min_dist and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dist = dist
        nearest_waypoint = waypoint
      end
    end
  end

  return nearest_waypoint
end

-- Finds waypoint nearest to 'point' within 'range'
function Map:nearestWaypointInRange(point, range)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local nearest_waypoint = false
  local min_dist = math.huge

  for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
    local dist = point:dist(waypoint.circle.position)
    -- if this is the closest we've found and we have LOS, set this as the nearest
    local lookAt = waypoint.circle.position - point

    if dist < min_dist and dist < range then
      min_dist = dist
      nearest_waypoint = waypoint
    end
  end

  return nearest_waypoint
end

function Map:addWall(points)
  table.insert(self.shapes, self.collider:addPolygon(unpack(self.polygon_points)))
end

-- HardonCollider delegate

function Map.on_collision(dt, shape_a, shape_b, mtv_x, mtv_y)
end

function Map.collision_stop()
end



-- AStar MapHandler

function Map:getNode(waypoint)

  if self.waypoints[waypoint.id] == nil then
    return nil
  end

  return Node(waypoint, 10, waypoint.id)
end


function Map:getAdjacentNodes(curnode, dest)
  local result = {}

  -- Process rooms according to door connections
  local currentWaypoint = self.waypoints[curnode.location.id]

  for neighbor_id, _ in pairs(currentWaypoint.connections) do
    local n = self:_handleNode(neighbor_id, curnode, dest.id)
    if n then
      table.insert(result, n)
    end
  end

  return result
end

function Map:locationsAreEqual(a, b)
  return a.id == b.id
end

function Map:_handleNode(id, fromnode, destid)
  -- Fetch a Node for the given location and set its parameters
  local waypoint = self.waypoints[id]
  local destWaypoint = self.waypoints[destid]

  local n = self:getNode(waypoint)

  if n ~= nil then
    local dx = math.max(waypoint.circle.position.x, destWaypoint.circle.position.x) - math.min(waypoint.circle.position.x, destWaypoint.circle.position.x)
    local dy = math.max(waypoint.circle.position.y, destWaypoint.circle.position.y) - math.min(waypoint.circle.position.y, destWaypoint.circle.position.y)
    local emCost = dx + dy

    n.mCost = n.mCost + fromnode.mCost
    n.score = n.mCost + emCost
    n.parent = fromnode

    return n
  end

  return nil
end

