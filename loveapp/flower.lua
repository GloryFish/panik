require 'middleclass'
require 'vector'
require 'states'
require 'notifier'
require 'circle'
require 'colors'

Flower = class('Flower')

function Flower:initialize(position, player)
  self.circle = Circle(position, 5)
  self.color = colors.green
  self.progress = 0
  self.state = States:getState('state_flower_get_attention')
  self.player = nil

  -- State variables
  self.attention = 0
  self.maxAttention = 3
  self.attentionRange = 20
  self.height = 0

end

function Flower:receiveMessage(message, data)
  if message == 'world_click' then
    local worldPoint = data
  end
end

function Flower:setState(state)
  if (self.state ~= state) then
    self.state:exit()
    self.state = state
    self.state:enter(self)
  end
end

function Flower:containsPoint(point)
  self.circle:containsPoint(point)
end

function Flower:setPosition(pos)
  self.circle.position = pos
end

function Flower:getPosition()
  return self.circle.position
end

function Flower:update(dt)
  self.state:update(self, dt)
end

function Flower:draw()
  self.color:set()
  self.circle:draw(nil, 'fill')
  love.graphics.line(self.circle.position.x, self.circle.position.y, self.circle.position.x, self.circle.position.y - self.height * 15)
end