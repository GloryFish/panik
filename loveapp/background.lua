require 'middleclass'
require 'vector'
require 'colors'
require 'rectangle'

Background = class('Background')

function Background:initialize()
  self.image = love.graphics.newImage('resources/sprites/stars.png')
  self.imageWidth = self.image:getWidth()
  self.imageHeight = self.image:getHeight()

  self.viewport = Rectangle(vector(0, 0), vector(love.graphics.getWidth(), love.graphics.getHeight()))

end

function Background:setViewport(rect)
  self.viewport  = rect
end


function Background:draw()
  local min = self.viewport:getMin()
  local max = self.viewport:getMax()

  local startx = (math.floor(min.x / self.imageWidth)) * self.imageWidth
  local endx = (math.floor(max.x / self.imageWidth)) * self.imageWidth

  local starty = (math.floor(min.y / self.imageHeight)) * self.imageHeight
  local endy = (math.floor(max.y / self.imageHeight)) * self.imageHeight

  colors.white:set()
  for x = startx, endx, self.imageWidth do
    for y = starty, endy, self.imageHeight do
      love.graphics.draw(self.image, x, y)
    end
  end
end
