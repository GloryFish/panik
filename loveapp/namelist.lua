require 'middleclass'

local NameList = class('NameList')


local femaleNames = {}
local maleNames = {}
local lastNames = {}

function NameList:initialize()
  local count = 0
  if #lastNames == 0 then
    for line in love.filesystem.lines('resources/data/female-names-unique.txt') do
      table.insert(femaleNames, line)
      count = count + 1
    end

    for line in love.filesystem.lines('resources/data/male-names-unique.txt') do
      table.insert(maleNames, line)
      count = count + 1
    end

    for line in love.filesystem.lines('resources/data/lastnames.txt') do
      table.insert(lastNames, line)
      count = count + 1
    end

    print('Loaded '..count..' names')
  end
end

function NameList:randomName(gender)
  local firstNames = femaleNames
  if gender == 'male' then
    firstNames = maleNames
  end

  local first = firstNames[math.random(#firstNames)]
  local last = lastNames[math.random(#lastNames)]

  return string.format('%s %s', first, last)
end

return NameList()