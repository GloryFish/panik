require 'middleclass'
require 'vector'
require 'states'
require 'notifier'
require 'circle'
require 'colors'

Player = class('Player')

function Player:initialize(position)
  self.circle = Circle(position, 20)
  self.color = colors.black

  self.state = States:getState('idle')
  self.movement = vector(0, 0) -- This holds a vector containing the last movement input received
  self.velocity = vector(0, 0)
  self.target = vector(0, 0)

  -- stats
  self.base_speed = 100
  self.vision_range = 600
  self.health = 1

  self.standardDecay = 0.02
  self.maslowBoost = self.standardDecay * 2
  self.maslowRange = (love.graphics.getWidth() / 2) + 75

  Notifier:listenForMessage('world_click', self)
end

function Player:receiveMessage(message, data)
  if message == 'world_click' then
    print('click')
    local worldPoint = data
    self:setTargetPoint(self.map:gridCenter(worldPoint))
  end
end

function Player:setState(state)
  if (self.state ~= state) then
    self.state:exit()
    self.state = state
    self.state:enter(self)
  end
end

function Player:setTargetPoint(target)
    self.target = target
    self.path = nil
    self:setState(States:getState('state_player_move_to_target'))
end

-- Call during update with a normalized movement vector
function Player:setMovement(movement)
  self.movement = movement
  self.velocity.x = movement.x * self.base_speed
  self.velocity.y = movement.y * self.base_speed

  if movement.x ~= 0 and movement.y ~= 0 then
    self.direction = self.movement
  end
end

function Player:containsPoint(point)
  self.circle:containsPoint(point)
end

function Player:setPosition(pos)
  self.circle.position = pos
end

function Player:getPosition()
  return self.circle.position
end

function Player:getAIMovement(target, map)
  local movement = vector(0, 0)

  if self.path == nil then
    self.path = map:pathBetween(self.circle.position, target, self.vision_range)
    if self.path == nil then
      self:setState(States:getState('idle'))
      return vector(0, 0)
    end
  end

  local nextLocation = self.path[1]

  -- Check to see if we've reached the current node
  if nextLocation ~= nil then
    if self.circle.position:dist(nextLocation) < 10 then
      table.remove(self.path, 1)
      nextLocation = self.path[1]
    end
  end

  -- Move towards our current node
  if nextLocation ~= nil then
    local movement = nextLocation - self.circle.position
    movement:normalize_inplace()
    return movement
  else
    print('idling')
      self:setState(States:getState('idle'))
      return vector(0, 0)
  end

  return movement
end

function Player:update(dt)
  self.state:update(self, dt)
  -- Apply velocity to position
  self.circle.position = self.circle.position + self.velocity * dt

  -- Standard decay
  self.health = self.health - self.standardDecay * dt

  -- Increase due to proximity with Maslow
  if self.circle.position:len() < self.maslowRange then
    self.health = self.health + self.maslowBoost * dt
  end

  if self.health > 1 then
    self.health = 1
  end
end

function Player:draw()
  self.color:set()
  self.circle:draw(nil, 'fill')
  colors.gray:set()

  colors.white:set()
  love.graphics.circle('fill', self.circle.position.x, self.circle.position.y, (1 - self.health) * self.circle.radius, 30)

  colors.red:set()
  love.graphics.circle('line', 0, 0, self.maslowRange, 60)

  if self.target then
    colors.pink:set()
    love.graphics.circle('line', self.target.x, self.target.y, 5, 50)
  end
end