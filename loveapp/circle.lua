--
--  circle.lua
--

require 'middleclass'
require 'vector'

Circle = class('Circle')
function Circle:initialize(position, radius)
  self.position = position
  self.radius = radius
end

function Circle:contains(point)
  return self.position:dist(point) <= self.radius
end


function Circle:center()
  return self.position
end

function Circle:AABB()
  return self.position.x - self.radius, self.position.y - self.radius, self.position.x + self.radius, self.position.y + self.radius
end

-- This is mainly for debugging
function Circle:draw(offset, mode)
  if offset == nil then
    offset = vector(0, 0)
  end

  if mode == nil then
    mode = 'line'
  end

  love.graphics.circle(mode, self.position.x + offset.x, self.position.y + offset.y, self.radius, 40)
end




