require 'middleclass'
require 'vector'
require 'colors'
require 'rectangle'
require 'room'

Station = class('Station')

function Station:initialize()
  self.rooms = {}
  local livingRoom = Room()
  local size = livingRoom:getSize()
  livingRoom:setPosition(vector(size.x / 2 * -1, size.y / 2 * -1))
  table.insert(self.rooms, livingRoom)

  self.entities = {}
end

function Station:addEntity(entity)
  table.insert(self.entities, entity)
end

function Station:getAdvertisements()
  local advertisments = {}
  for index, entity in ipairs(self.entities) do
    for index, ad in ipairs(entity:getAdvertisements()) do
      table.insert(advertisments, ad)
    end
  end
  return advertisments
end


function Station:draw()
  for index, room in ipairs(self.rooms) do
    room:draw()
  end
end
