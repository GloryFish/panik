--
--  commands.lua
--  rogue-descent
--
--  A set of commands for rogue-descent.
--
--  Created by Jay Roberts on 2012-04-27.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'utility'
require 'vector'

local commands = {

  shader = {
    description = 'Set the current pixel shader effect',
    callback = function(delegate, ...)
      local name = arg[1]

      if name == nil then
        current_effect = nil
        return 'No effect'
      end

      if name == 'list' then
        local items = {}
        for shaderName, shader in pairs(shaders.shaders) do
          table.insert(items, shaderName)
        end
        return items
      end

      if name == 'reload' then
        shaders:reload()
        return 'Reloaded shaders'
      end


      if not shaders:isEffect(name) then
        return 'Invalid effect: '..name
      end

      current_effect = name
      return 'Effect: '..name
    end,
  },

  varset = {
    description = 'Set game variables',
    callback = function(delegate, ...)
      local name = arg[1]
      local val = arg[2]

      local translated = val

      if val == 'true' then
        translated = true
      elseif val == 'false' then
        translated = false
      elseif tonumber(val) ~= nil then
        translated = tonumber(val)
      end

      vars[name] = translated
      return 'set variable'
    end,
  },

  varget = {
    description = 'Get game variables',
    callback = function(delegate, ...)
      local name = arg[1]
      return tostring(vars[name])
    end,
  },

  varlist = {
    description = 'List game variables',
    callback = function(delegate, ...)
      local names = {}
      for name, val in pairs(vars) do
        table.insert(names, name)
      end
      return names
    end,
  },

  memory = {
    description = 'Get memory usage info',
    callback = function(delegate, ...)
      return 'Memory used: '..tostring(collectgarbage('count'))..'kb'
    end,
  },

  debug = {
    description = 'Toggle debug mode',
    callback = function(delegate, ...)
      vars.debug = not vars.debug
      vars.showgraph = not vars.showgraph
      vars.showhash  = not vars.showhash
      vars.showwalls = not vars.showwalls
      vars.showpaths = not vars.showpaths
      vars.showstats = not vars.showstats
      vars.showai = not vars.showai
      vars.enableeditor = not vars.enableeditor
      if vars.debug then
        return 'Debug mode: ON'
      else
        return 'Debug mode: OFF'
    end
    end,
  },

  kill = {
    description = 'Kill the player',
    callback = function(delegate, ...)
      delegate.player:takeDamage(10000)
    end,
  },
}

return commands