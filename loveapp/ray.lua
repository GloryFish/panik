
local setmetatable, getmetatable = setmetatable, getmetatable
local assert, type, tonumber = assert, type, tonumber
local sqrt, cos, sin, min, max = math.sqrt, math.cos, math.sin, math.min, math.max
local love = love

module(...)

local ray = {}
ray.__index = ray

function new(x, y, dx, dy)
  local r = {x = x or 0, y = y or 0, dx = dx or 0, dy = dy or 0}
  setmetatable(r, ray)
  return r
end

function isray(r)
  return getmetatable(r) == ray
end

function ray:clone()
  return new(self.x, self.y, self.dx, self.dy)
end

function ray:unpack()
  return self.x, self.y, self.dx, self.dy
end

function ray:__tostring()
  return string.format('(%d, %d, %d, %d)', self.x, self.y, self.dx, self.dy)
end

function ray.__eq(a,b)
  return a.x == b.x and a.y == b.y and a.dx == b.dx and a.dy == b.dy
end

function ray:origin()
  return self.x, self.y
end

function ray:lookAt()
  return self.dx, self.dy
end

function ray:AABB()
  return min(self.x, self.x + self.dx), min(self.y, self.y + self.dy), max(self.x, self.x + self.dx), max(self.y, self.y + self.dy)
end

function ray:draw()
  love.graphics.line(self.x, self.y, self.x + self.dx, self.y + self.dy)
  love.graphics.circle('line', self.x + self.dx, self.y + self.dy, 3, 10)
end

function ray:drawAABB()
  local x1, y1, x2, y2 = self:AABB()
  love.graphics.rectangle('line', x1, y1, x2 - x1, y2 - y1)
end


-- ray() as shortcut to ray.new()
do
  local m = {}
  m.__call = function(_, ...) return new(...) end
  setmetatable(_M, m)
end
