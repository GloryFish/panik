--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'player'
require 'camera'
require 'map'
require 'flower'

local scene = Gamestate.new()

function scene:enter(pre)
  love.graphics.setBackgroundColor(255, 255, 255)

  self.map = Map()

  self.player = Player(vector(0, 0))
  self.player.map = self.map

  self.flowers = {}

  for i = 1, 30 do
    local pos = vector(math.random(-500, 500), math.random(-500, 500))
    pos = self.map:gridCenter(pos)
    local flower = Flower(pos)
    flower.player = self.player
    table.insert(self.flowers, flower)
  end


  self.camera = Camera()
  self.camera.position = vector(0, 0)
  self.camera.focus = vector(0, 0)
  self.camera.deadzone = 0
  self.camera.smoothMovement = true
  self.camera.handheld = false
  self.camera.scale = 1
  self.camera.useBounds = false

  self.map.camera = self.camera
end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
  local worldPoint = self.camera:screenToWorld(vector(x, y))
  -- Pass this on to other interested game objects
  Notifier:postMessage('world_click', worldPoint)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  if love.mouse.isDown('l') then
  end

  Timer.update(dt)

  self.map:update(dt)
  self.player:update(dt)

  for index, flower in ipairs(self.flowers) do
    flower:update(dt)
  end

  self.camera.focus = self.player:getPosition()
  self.camera:update(dt)
end

function scene:draw()
  love.graphics.clear()

  self.camera:apply()

  self.map:draw()

  self.player:draw()

  -- Draw flowers
  for index, flower in ipairs(self.flowers) do
    flower:draw()
  end

  -- Draw maslow
  colors.purple:set()
  love.graphics.circle('fill', 0, 0, 10, 30)

  self.camera:unapply()

end

function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene