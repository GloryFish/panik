require 'middleclass'
require 'vector'
require 'states'
require 'notifier'
require 'namelist'

Agent = class('Agent')

function Agent:initialize(station)
  self.spritesheet = Sprites.main

  -- World
  self.station = station

  -- Agent person information
  self.name = NameList:randomName('male')

  -- Size and position
  self.position = vector(0, 0)
  self.rotation = 0
  self.scale = 1
  self.flip = 1
  self.offset = vector(0, 0)

  -- Animation
  self.currentFrameIndex = 1
  self.animationName = 'agent_1_idle'
  self.elapsed = 0

  -- Needs
  self.needs = {
    fatigue = 100,
    hunger = 100,
    bathroom = 100,
    boredom = 100,
    discomfort = 100,
    fear = 100,
  }

  -- Actions
  self.actions = {}

  Notifier:listenForMessage('world_down', self)
  Notifier:listenForMessage('sim_tick', self)

  self.state = States:getState('idle')
  self:setState(States:getState('select_action'))
end

function Agent:receiveMessage(message, data)
  if message == 'world_down' then
    local position = data
    if self:containsPoint(position) then
      Notifier:postMessage('agent_selected', self)
    end
  elseif message == 'sim_tick' then
    self:tick()
  end
end

function Agent:containsPoint(point)
  local size = self:getCurrentSize()
  return point.x >= self.position.x and
         point.x <= self.position.x + size.x and
         point.y >= self.position.y and
         point.y <= self.position.y + size.y
end

function Agent:getName()
  return self.name
end

function Agent:changeNeed(name, amount)
  self.needs[name] = self.needs[name] + amount
  if self.needs[name] < -100 then
    self.needs[name] = -100
  elseif self.needs[name] > 100 then
    self.needs[name] = 100
  end
end

function Agent:setPosition(pos)
  self.position = pos
end

function Agent:addAction(action)
  action:setAgent(self)
  table.insert(self.actions, action)
end

function Agent:setState(state)
  if (self.state ~= state) then
    self.state:exit()
    self.state = state
    self.state:enter(self)
  end
end

function Agent:getCurrentSize()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Agent:update(dt)
  self.state:update(dt)

  -- Handle animation
  self.elapsed = self.elapsed + dt
  local animation = self.spritesheet.animations[self.animationName]
  if #animation.frames > 1 then -- More than one frame
    local duration = animation.frames[self.currentFrameIndex].duration

    if self.elapsed > duration then -- Switch to next frame
      self.currentFrameIndex = self.currentFrameIndex + 1
      if self.currentFrameIndex > #animation.frames then -- Aaaand back around
        self.currentFrameIndex = 1
      end
      self.elapsed = self.elapsed - duration
    end
  end
end

function Agent:tick()
    -- fatigue
    self.needs.fatigue = self.needs.fatigue - 1

    -- hunger
    self.needs.hunger = self.needs.hunger - 3
    if self.needs.fear < 0 then
      self.needs.hunger = self.needs.hunger + (self.needs.fear / 100) * ((self.needs.hunger + 100) / 100)
    end

    -- bathroom
    self.needs.bathroom = self.needs.bathroom - 0.2

    -- boredom
    self.needs.boredom = self.needs.boredom - 0.1

    -- discomfort
    -- noop

    -- fear
    -- noop

end

function Agent:draw()
  colors.white:set()

  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  self.spritesheet.batch:addq(self.spritesheet.quads[currentFrame.name],
                              self.position.x,
                              self.position.y,
                              animation.rotation - self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)
end