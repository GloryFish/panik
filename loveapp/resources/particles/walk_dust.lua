--
-- particle_effect.lua
--

require 'middleclass'
require 'vector'

local ParticleEffect = class('ParticleEffect')

function ParticleEffect:initialize()
  self.systems = {}
  self.position = vector(0, 0)
  self.layer = 'below'
  self.active = true
  self.rate = 3

  -- Create and configure the systems that are part of this effect
  local dustImage = love.graphics.newImage('resources/sprites/debris.png');
  local system = love.graphics.newParticleSystem(dustImage, 500)

  system:setEmissionRate(self.rate)
  system:setSpeed(0, 0)
  system:setGravity(0)
  system:setSizes(0.4, 0.8)
  system:setColors(255, 255, 255, 130, 58, 128, 255, 0)
  system:setPosition(self.position.x, self.position.y)
  system:setLifetime(-1)
  system:setParticleLife(2)
  system:setDirection(math.pi / 2)
  system:setRotation(0, math.pi)
  system:setSpread(20)

  table.insert(self.systems, system)
end

function ParticleEffect:walk()
  for index, system in ipairs(self.systems) do
    -- system:setEmissionRate(self.rate)
    system:start()
  end
end

function ParticleEffect:stop()
  for index, system in ipairs(self.systems) do
    -- system:setEmissionRate(0)
    system:stop()
  end
end

function ParticleEffect:update(dt)
  for index, system in ipairs(self.systems) do
    -- TODO self.particles:setPosition(self.position.x, self.position.y)
    system:setPosition(self.position.x, self.position.y)
    system:update(dt)
  end
end


function ParticleEffect:draw()
  for index, system in ipairs(self.systems) do
    love.graphics.draw(system)
  end
end

return ParticleEffect