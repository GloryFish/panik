local animations = {
  entity_default = {
    rotation = 0,
    frames = {
      {
        name = 'object_rock.png',
        duration = 100,
      }
    },
  },

  entity_apple_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_apple.png',
        duration = 100,
      }
    },
  },

  agent_1_idle = {
    rotation = 0,
    frames = {
      {
        name = 'agent_1_stand.png',
        duration = 5,
      },
      {
        name = 'agent_1_blink.png',
        duration = 0.15,
      },
      {
        name = 'agent_1_stand.png',
        duration = 7,
      },
      {
        name = 'agent_1_blink.png',
        duration = 0.15,
      },
      {
        name = 'agent_1_stand.png',
        duration = 6,
      },
      {
        name = 'agent_1_blink.png',
        duration = 0.15,
      },
    },
  }
}

return animations