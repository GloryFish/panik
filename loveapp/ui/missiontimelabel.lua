require 'middleclass'
require 'ui/statbar'

MissionTimeLabel = class('MissionTimeLabel')

function MissionTimeLabel:initialize()
  self.startTime = 0
  self.textItem = loveframes.Create('text')
  self.textItem:SetText('00:00:00')

  self.textItem.Update = function(text, dt)
    local now = love.timer.getTime()
    local elapsed = now - self.startTime

    local hours =  elapsed / 3600
    local minutes =  (elapsed / 60) % 60
    local seconds =  elapsed % 60

    text:SetText(string.format('Mission Time: %02d:%02d:%02d', hours, minutes, seconds))
  end
end

function MissionTimeLabel:start()
  self.startTime = love.timer.getTime()
end


