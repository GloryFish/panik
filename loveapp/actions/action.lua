require 'middleclass'


Action = class('Action')

function Action:initialize()
end

function Action:getName()
end

function Action:setAgent(agent)
  self.agent = agent
end

function Action:update(dt)
end

function Action:isDone()
  return true
end