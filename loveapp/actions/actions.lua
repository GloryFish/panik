require 'middleclass'

Action = class('Action')

function Action:initialize()
end

function Action:update(dt)
end

function Action:isFinished()
  return true
end


CompositeAction = class('CompositeAction', Action)

function CompositeAction:initialize()
  self.actions = {}
end

function CompositeAction:add(action)
  table.insert(self.actions, action)
end


ParallelAction = class('ParallelAction', CompositeAction)

function ParallelAction:update(dt)
  for action in ipairs(self.actions) do
    action:update(dt)
  end



end

function ParallelAction:isFinished()
  return true
end
