require 'middleclass'
require  'actions/action'

ActionMoveTo = class('ActionMoveTo', Action)

function ActionMoveTo:initialize(target)
  Action.initialize(self)

  self.target = target
end

function ActionMoveTo:getName()
  return 'MoveTo: ' .. tostring(self.target)
end

function ActionMoveTo:update(dt)
  local movement = self.target - self.agent.position

  self.agent:setPosition(self.agent.position + (movement:normalized() * 10 * dt))
  self.agent.flip = 1
  if movement.x < 0 then
    self.agent.flip = -1
  end
end

function ActionMoveTo:isDone()
  local dir = self.target - self.agent.position

  return dir:len() < 10
end