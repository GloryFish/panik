require 'middleclass'
require 'vector'
require 'colors'
require 'rectangle'

Room = class('Room')

function Room:initialize()
  self.position = vector(0, 0)
  self.size = vector(200, 200)
  self.rectangle = Rectangle(self.position, self.size)
  self.color = Color(255, 145, 50)
end

function Room:getSize()
  return self.size
end

function Room:setPosition(position)
  self.position = position
  self.rectangle.position = position
end

function Room:draw()
  self.color:set()
  self.rectangle:draw(nil, 'fill')
end
