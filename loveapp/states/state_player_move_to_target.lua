require 'middleclass'
require 'vector'

local StatePlayerMoveToTarget = class('StatePlayerMoveToTarget')

function StatePlayerMoveToTarget:initialize(actor)
end

function StatePlayerMoveToTarget:enter(actor)
end

function StatePlayerMoveToTarget:update(actor, dt)
  if actor.target == nil then
    actor:setState(States:getState('idle'))
  elseif actor.target:dist(actor:getPosition()) < 3 then
    actor:setState(States:getState('idle'))
  else
    actor:setMovement(actor:getAIMovement(actor.target, actor.map))
  end
end

function StatePlayerMoveToTarget:exit(actor)

end

return StatePlayerMoveToTarget