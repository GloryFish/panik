require 'middleclass'

local StateFlowerGetAttention = class('StateFlowerGetAttention')

function StateFlowerGetAttention:initalize()
end

function StateFlowerGetAttention:enter(actor)
  actor:setMovement(vector(0, 0))
end

function StateFlowerGetAttention:update(actor, dt)
  local apos = actor:getPosition()
  local ppos = actor.player:getPosition()

  if apos:dist(ppos) < actor.attentionRange then
    actor.attention = actor.attention + dt
  end

  if actor.attention > actor.maxAttention then
    if (math.random() < 0.9) then
      actor.height = actor.height + 1
      if actor.height > 4 then
        return
      end

      actor.attention = 0
    else
      actor:setState(States:getState('state_flower_die'))
    end
  end
end

function StateFlowerGetAttention:exit()
end

return StateFlowerGetAttention
