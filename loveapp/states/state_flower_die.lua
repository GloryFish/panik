require 'middleclass'
require 'colors'

local StateFlowerDie = class('StateFlowerDie')

function StateFlowerDie:initalize()
end

function StateFlowerDie:enter(actor)
  print('dying')
  actor.color = colors.brown
end

function StateFlowerDie:update(actor, dt)
end

function StateFlowerDie:exit()
end

return StateFlowerDie
