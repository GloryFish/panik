require 'middleclass'

local StateFlowerGrow = class('StateFlowerGrow')

function StateFlowerGrow:initalize()
end

function StateFlowerGrow:enter(actor)
  print('growing')
  Timer.addPeriodic(1, function()
    actor.height = actor.height + 1
    if actor.height == 4 then
      return false
    end
  end)
end

function StateFlowerGrow:update(actor, dt)
end

function StateFlowerGrow:exit()
end

return StateFlowerGrow
