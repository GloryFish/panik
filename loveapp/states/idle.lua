require 'middleclass'

local StateIdle = class('StateIdle')

function StateIdle:initalize()
end

function StateIdle:enter(actor)
  actor:setMovement(vector(0, 0))
end

function StateIdle:update(actor, dt)
end

function StateIdle:exit()
end

return StateIdle
