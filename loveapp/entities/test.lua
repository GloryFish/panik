-- Test

local IndividualEntity = class('EntityTest', Entity)

function IndividualEntity:initialize()
  Entity.initialize(self)
end

function IndividualEntity:__tostring()
  return 'EntityTest'
end

return IndividualEntity