-- Apple

require 'middleclass'
require 'actions/moveto'
require 'actions/custom'

local EntityApple = class('EntityApple', Entity)


function EntityApple:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_apple_idle'

  self.eaten = false
end

function EntityApple:getAdvertisements()
  if self.eaten then
    return {}
  else

    local ads = {
      {
        name = 'eat',
        reward = 'hunger',
        amount = 30,
        actions = {
          ActionMoveTo(self.position),
          ActionCustom('Eat Apple',
            function(this, dt) -- update
              this.agent:changeNeed('hunger', 30)
              self.eaten = true
            end,
            function() -- done
              return self.entity.eaten
            end
           ),
        }
      },
    }

    return ads
  end
end

function EntityApple:draw()
  if not self.eaten then
    Entity.draw(self)
  end
end

return EntityApple